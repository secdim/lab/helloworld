FROM node:8.16.2-alpine as build
MAINTAINER secdim
ARG TESTSPEC="usability"
# Create app directory
WORKDIR /usr/src/app
# Install app dependencies
COPY src/package.json .
# For npm@5 or later, copy package-lock.json as well
# COPY package.json package-lock.json ./
RUN ["npm","install"]
# Bundle app source
COPY src .
COPY test .
# Run tests
RUN ["npm","run","test","--", "--forceExit","-t","$TESTSPEC"]


FROM node:8.16.2-alpine
# HTTP listener port
ENV PORT 8080
# Create app directory
WORKDIR /usr/src/app
COPY --from=build /usr/src/app .
EXPOSE 8080
# Drop to low priv user
USER nobody
CMD [ "node", "app.js" ]
