APP=secdim.lab.prep

all: build

test:
	docker build --target build --build-arg TESTSPEC=usability --rm .

securitytest:
	docker build --target build --build-arg TESTSPEC=security --rm .

build:
	docker build --rm --tag=$(APP) .

run:
	docker run -p 8080:8080 --rm $(APP)

clean:
	docker image rm $(APP)
	docker system prune

.PHONY: all test securitytest clean
