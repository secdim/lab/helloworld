= Hello World
A preparation lab for Defensive Programming/Testing Training course by https://secdim.com[SecDim].

This lab runs a small web application in Docker.

== Objective

See "Hello World!" message when you browse 
to http://localhost:8080

== Structure

* `src`: the program code. 
* `test`: unit and security tests.
* `MakeFile`: instructions for make utility.
* `DockerFile`: instructions for Docker.

== Task 0

Setup the following tools:

. https://docs.docker.com/install/[docker]
. https://www.gnu.org/software/make/[make]
. https://docs.docker.com/compose/install/[docker-compose]

== Task 1

Clone this repository.

== Task 2

Build the program with `make build`. 
This will pull the required docker image and installs dependencies.
It will then run unit tests.

== Task 3

Run the program with `make run`

== Task 4

`curl http://localhost:8080`.
You should see "Hello World!"

== Task 5

Run unit tests with `make test`. 
Run security unit tests with `make securitytest`.
All tests should pass. The security tests
for this lab is dummy.

== Task 6

Congratulations!
You are ready for your training class.

== Troubleshooting

=== Error response from daemon: network mode buildkit

If you receive the following error: `Error response from daemon: network mode "xyz" not supported by buildkit`
type the following in your terminal: `export DOCKER_BUILDKIT=0`
and run the command again.

=== make fails with error message

Have a look at `MakeFile`. Review the commands.
You can copy and paste the commands
from `MakeFile` and run them separately. 

=== make does not do anything

Run `make clean` and then run your desired make option. 

=== make run does not reflect recent changes

Run `make build` and then `make run`.
